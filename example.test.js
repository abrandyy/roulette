const http = require('http');
const rp = require('request-promise');
const apiUrl = 'https://roulette-api.nowakowski-arkadiusz.com';

// Helper to make the code shorter
const post = (path, hashname = '', body = {}) => rp({
    method: 'POST',
    uri: apiUrl + path,
    body: body,
    json: true,
    headers: { 'Authorization': hashname }
});

// Helper to make the code shorter
const get = (path, hashname = '') => rp({
    method: 'GET',
    uri: apiUrl + path,
    json: true,
    headers: { 'Authorization': hashname }
});

test('Player should have 100 chips', () => {
    let hashname;
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return get('/chips', hashname);
    })
    .then(response => expect(response.chips).toEqual(100))
});

for (let number of [2,4,6,8,10]) {
    test('Black should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/black', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(200))
    });
}

test.each([1,3,5])('Bet on odds should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/odd', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});
