const http = require('http');
const rp = require('request-promise');
const apiUrl = 'https://roulette-api.nowakowski-arkadiusz.com';

// Helper to make the code shorter
const post = (path, hashname = '', body = {}) => rp({
        method: 'POST',
        uri: apiUrl + path,
        body: body,
        json: true,
        headers: {'Authorization': hashname}
   });

// Helper to make the code shorter
const get = (path, hashname = '') => rp({
        method: 'GET',
        uri: apiUrl + path,
        json: true,
        headers: {'Authorization': hashname}
   });

//test('Type your tests here', () => {});
test('Player should have 100 chips', () => {
    let hashname;
    return post('/players')
            .then(response => {
                hashname = response.hashname;
                return get('/chips', hashname);
            })
            .then(response => expect(response.chips).toEqual(100))
});

for (let number of [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 19, 20, 22, 24, 26, 29, 31, 33, 35]) {
    test('Black should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname;
                    return post('/bets/black', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(200))
   })};


for (let number of [1, 3, 5, 7, 9, 12, 14, 16, 18, 21, 23, 25, 27, 30, 28, 32, 34, 36]) {
    test('Red should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname;
                    return post('/bets/red', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(200))
   })};



for (let number of [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34]) {
    test('Column 1, numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname;
                    return post('/bets/column/1', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(300))
   })};


for (let number of [2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35]) {
    test('Column 2, numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname;
                    return post('/bets/column/2', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(300))
   })};


for (let number of [3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36]) {
    test('Column 3, numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname;
                    return post('/bets/column/3', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(300))
   })};


for (let number of [1, 2, 4, 5]) {
    test('Corner 1-2-4-5, numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname;
                    return post('/bets/corner/1-2-4-5', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
   })};


for (let number of [4, 5, 7, 8]) {
    test('Corner 4-5-7-8, numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname;
                    return post('/bets/corner/4-5-7-8', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
   })};


var corners = ['1-2-4-5', '5-6-8-9', '2-3-5-6', '4-5-7-8', '7-8-10-11', '8-9-11-12',
    '10-11-13-14', '11-12-14-15', '13-14-16-17', '14-15-17-18', '16-17-19-20',
    '17-18-20-21', '19-20-22-23', '20-21-23-24', '22-23-25-26', '23-24-26-27', '25-26-28-29',
    '26-27-29-30', '28-29-31-32', '29-30-32-33', '31-32-34-35', '32-33-35-36', '0-1-2-3'];

for (let number of [1, 2, 4, 5]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/1-2-4-5', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [2, 3, 5, 6]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/2-3-5-6', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [4, 5, 7, 8]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/4-5-7-8', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [5, 6, 8, 9]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/5-6-8-9', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;
for (let number of [7, 8, 10, 11]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/7-8-10-11', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [8, 9, 11, 12]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/8-9-11-12', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [10, 11, 13, 14]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/10-11-13-14', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [11, 12, 14, 15]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/11-12-14-15', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [13, 14, 16, 17]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/13-14-16-17', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [14, 15, 17, 18]) {
    test('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/14-15-17-18', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [16, 17, 19, 20]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/16-17-19-20', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [17, 18, 20, 21]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/17-18-20-21', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [19, 20, 22, 23]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/19-20-22-23', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [20, 21, 23, 24]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/20-21-23-24', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [22, 23, 25, 26]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/22-23-25-26', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [23, 24, 26, 27]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/23-24-26-27', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [25, 26, 28, 29]) {
    test('Corner  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/25-26-28-29', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [26, 27, 29, 30]) {
    test('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/corner/26-27-29-30', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
   })};

    for (let number of [28, 29, 31, 32]) {
        test('Corner  numer:' + number + ' is spinned', () => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/corner/28-29-31-32', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(900))
        })
    }
    ;

    for (let number of [29, 30, 32, 33]) {
        test('Corner  numer:' + number + ' is spinned', () => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/corner/29-30-32-33', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(900))
        })
    }
    ;


    for (let number of [31, 32, 34, 35]) {
        test('Corner  numer:' + number + ' is spinned', () => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/corner/31-32-34-35', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(900))
        })
    }
    ;

    for (let number of [32, 33, 35, 36]) {
        test('Corner  numer:' + number + ' is spinned', () => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/corner/32-33-35-36', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
        })
    }
    ;

    for (let number of [0, 1, 2, 3]) {
        test('Corner  numer:' + number + ' is spinned', () => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/corner/0-1-2-3', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(900))
        })}


for (let number of [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]) {
    test('Dozen 1,2,4,5,6,7,8,9,10,11,12, numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/dozen/1', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(300))
    })
}
;


for (let number of [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]) {
    test('Dozen 13,14,15,16,17,18,19,20,21,22,23,24, numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname;
                    return post('/bets/dozen/2', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(300))
   })};
for (let number of [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36]) {
    test('Dozen 25,26,27,28,29,30,31,32,33,34,35,36 numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname;
                    return post('/bets/dozen/3', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(300))
   })};



for (let number of [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36]) {
    test('Even numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/even', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(200))
    })
}
;

for (let number of [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36]) {
   test ('Even numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/even', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(200))
    })
}
;

for (let number of [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35]) {
   test ('Even numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/odd', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(200))
    })
}
;

for (let number of [1, 2, 3, 4, 5, 6]) {
    test('Line  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/line/1-2-3-4-5-6', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(600))
    })
}
;

for (let number of [1, 2, 3, 4, 5, 6]) {
    test('Line  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/line/1-2-3-4-5-6', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(900))
    })
}
;

for (let number of [4, 5, 6, 7, 8, 9]) {
    test('Line  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/line/4-5-6-7-8-9', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(600))
    })
}
;

for (let number of [7, 8, 9, 10, 11, 12]) {
    test('Line  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/line/7-8-9-10-11-12', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(600))
    })
}
;

for (let number of [10, 11, 12, 13, 14, 15]) {
    test('Line  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/line/10-11-12-13-14-15', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(600))
    })
}
;

for (let number of [16, 17, 18, 19, 20, 21]) {
    test('Line  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/line/16-17-18-19-20-21', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(600))
    })
}
;

for (let number of [19, 20, 21, 22, 23, 24]) {
    test('Line  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/line/19-20-21-22-23-24-25', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(600))
    })
}
;

for (let number of [22, 23, 24, 25, 26, 27]) {
    test('Line  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/line/22-23-24-25-26-27', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(600))
    })
}
;

for (let number of [25, 26, 27, 28, 29, 30]) {
    test('Line  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/line/25-26-27-28-29-30', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(600))
    })
}
;

for (let number of [28, 29, 30, 31, 32, 33]) {
    test('Line  numer:' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/line/28-29-30-31-32-33', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(600))
   })};

    for (let number of [31, 32, 33, 34, 35, 36]) {
        test('Line  numer:' + number + ' is spinned', () => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/line/31-32-33-34-35-36', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(600))
        })
    }
    ;

    for (let number of [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]) {
       test ('Low numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/low', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(200))
        })
    }
    ;

    for (let number of [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]) {
       test ('Low numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/low', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(200))
        })
    }
    ;

    for (let number of[0, 1]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/0-1', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[1, 2]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/1-2', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[1, 4]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/1-4', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[2, 5]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/2-5', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[3, 6]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/3-6', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[2, 3]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/2-3', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
       })};

    for (let number of[4, 7]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/4-7', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[5, 8]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/5-8', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[6, 9]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/6-9', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[3, 4]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/3-4', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[7, 10]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/7-10', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[8, 11]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/8-11', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[9, 12]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/9-12', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[4, 5]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/4-5', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[10, 13]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/10-13', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[11, 14]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/11-14', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
    }
    ;

    for (let number of[12, 15]) {
       test ('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/12-15', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })
}
;

for (let number of[5, 6]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/5-6', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[13, 16]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/13-16', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[14, 17]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/14-17', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[15, 18]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/15-18', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[6, 7]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/6-7', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[16, 19]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/16-19', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[17, 20]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/17-20', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[18, 21]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/18-21', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[7, 8]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/7-8', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[19, 22]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/19-22', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[20, 23]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/20-23', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[21, 24]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/21-24', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[8, 9]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/8-9', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[22, 25]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/22-25', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[23, 26]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/23-26', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[24, 27]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/24-27', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[9, 10]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/9-10', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[25, 28]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/25-28', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[26, 29]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/26-29', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[27, 30]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/27-30', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[10, 11]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/10-11', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[28, 31]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/28-31', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[29, 32]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/29-32', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[30, 33]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/30-33', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[11, 12]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/11-12', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[31, 34]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/31-34', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[32, 35]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/32-35', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[33, 36]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/33-36', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[12, 13]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/12-13', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[13, 14]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/13-14', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[14, 15]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/14-15', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[15, 16]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/15-16', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[16, 17]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/16-17', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[17, 18]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/17-18', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[18, 19]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/18-19', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })};

for (let number of[19, 20]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/19-20', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })};

for (let number of[20, 21]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/20-21', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[21, 22]) {
   test ('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/21-22', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[22, 23]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/22-23', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })};

for (let number of[23, 24]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/23-24', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};


for (let number of[24, 25]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/24-25', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[25, 26]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/25-26', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[26, 27]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/26-27', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[27, 28]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/27-28', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

for (let number of[28, 29]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/28-29', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[29, 30]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/29-30', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[30, 31]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/30-31', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })};

for (let number of[31, 32]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/31-32', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })};

for (let number of[32, 33]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/32-33', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

for (let number of[33, 34]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/33-34', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
   })};

    for (let number of[34, 35]) {
         test('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/34-35', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
       })};

    for (let number of[35, 36]) {
         test('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/35-36', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
       })};

    for (let number of[0, 2]) {
         test('Spit numer: %d is spinned', number => {
            return post('/players')
                    .then(response => {
                        hashname = response.hashname
                        return post('/bets/split/0-2', hashname, {chips: 100});
                    })
                    .then(response => post('/spin/' + number, hashname))
                    .then(response => get('/chips', hashname))
                    .then(response => expect(response.chips).toEqual(1800))
        })}

;

for (let number of[0, 3]) {
     test('Spit numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/split/0-3', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1800))
    })
}
;

//list.forEach(testEven);

for (let number of [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36]) {
     test('Straight numer: %d is spinned', number => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/straight/' + number, hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(3600))
    })
}
;

for (let number of [1, 2, 3]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/1-2-3', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;

for (let number of [4, 5, 6]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/4-5-6', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;


for (let number of [7, 8, 9]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/7-8-9', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;


for (let number of [10, 11, 12]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/10-11-12', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;


for (let number of [13, 14, 15]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/13-14-15', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;

for (let number of [16, 17, 18]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/16-17-18', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;

for (let number of [19, 20, 21]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/19-20-21', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;


for (let number of [22, 23, 24]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/22-23-24', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;

for (let number of [25, 26, 27]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/25-26-27', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;

for (let number of [28, 29, 30]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/28-29-30', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;

for (let number of [31, 32, 33]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/31-32-33', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;

for (let number of [34, 35, 36]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/34-35-36', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;

for (let number of [0, 1, 2]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/0-1-2', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;


for (let number of [0, 2, 3]) {
    test('Street ' + number + ' is spinned', () => {
        return post('/players')
                .then(response => {
                    hashname = response.hashname
                    return post('/bets/street/0-2-3', hashname, {chips: 100});
                })
                .then(response => post('/spin/' + number, hashname))
                .then(response => get('/chips', hashname))
                .then(response => expect(response.chips).toEqual(1200))
    })
}
;

